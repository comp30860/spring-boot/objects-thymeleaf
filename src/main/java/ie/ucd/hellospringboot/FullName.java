package ie.ucd.hellospringboot;

import java.io.Serializable;

public class FullName implements Serializable {
    private static final long serialVersionUID = 1L;

    private String firstName;
    private String surname;

    public FullName() {}
    
    public FullName(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }
}