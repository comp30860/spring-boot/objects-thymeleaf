package ie.ucd.hellospringboot;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
    @GetMapping("/names")
    public String names(Model model) {
        ArrayList<FullName> names = new ArrayList<FullName>();
        names.add(new FullName("Rem", "Collier"));
        names.add(new FullName("Eoin", "O'Neill"));
        model.addAttribute("names", names);
        return "hello";
    }
}